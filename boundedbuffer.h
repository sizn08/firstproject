#ifndef BOUNDEDBUFFER_H
#define BOUNDEDBUFFER_H

#include "synch.h"
#include "list.h"
#include "thread.h"

class BoundedBuffer {
public:
    BoundedBuffer(int maxsize);
    ~BoundedBuffer();

    int Read(void* data, int size, int n);

    int Write(void* data, int size, int n);

    void Close();

private:
    int count;
    int maxsize;
    bool reading;
    bool writing;

    char* buffer;
    char* in;
    char* out;
    Lock* lock;
    Condition* bufferEmpty;
    Condition* bufferFull;
    Condition* onerequestRead; 
    Condition* onerequestWrite;
};


#endif
