#include "boundedbuffer.h"
#include "synch.h"
#include "copyright.h"

BoundedBuffer::BoundedBuffer(int maxsize)
{
    this->maxsize = maxsize;
    count = 0;

    buffer = new char[maxsize];
    in = buffer;
    out = buffer;
    lock = new Lock("buffer lock");
    bufferEmpty = new Condition("buffer empty cond");
    bufferFull = new Condition("buffer full cond");
    onerequestRead = new Condition("requested have been read");
    onerequestWrite = new Condition("requested have been write");
    reading = false;
    writing = false;
}

BoundedBuffer::~BoundedBuffer()
{
    delete bufferEmpty;
    delete bufferFull;
    delete onerequestRead;
    delete onerequestWrite;
    delete lock;
    delete[] buffer;
}

int BoundedBuffer::Read(void* data, int size, int n)
{
    lock->Acquire();
    if(reading) onerequestRead->Wait(lock);

    cout << "reader - " << n+1 << " start " << size << " size read\n\n";
    reading = true;
    for(int i = 0; i < size; i++){
	while(count == 0){
	    cout << "reader - " << n+1 << " wait until buffer full\n\n";
	    bufferFull->Wait(lock);
	}
	*((char*)data + i) = *out;	
	if(out - buffer == maxsize - 1) out = buffer;
	else out++;
        count--;
	cout << "reader - " << n+1 << " read 1, buffer " << count << " left\n";
    }
    reading = false;
    cout << "\nreader - " << n+1 << " completed " << size << " size read\n\n";

    bufferEmpty->Signal(lock);
    onerequestRead->Broadcast(lock);

    lock->Release();
    return size;
}

int BoundedBuffer::Write(void* data, int size, int n)
{
    lock->Acquire();
    if(writing) onerequestWrite->Wait(lock);

    cout << "writer - " << n+1 << " start " << size << " size write\n\n";
    writing = true;
    for(int j=0; j<size; j++){
        while(count == maxsize){
	   cout << "writer - " << n+1 << " wait until buffer empty\n\n";
           bufferEmpty->Wait(lock);
	}
        *in = *((char*)data + j);
	if(in - buffer == maxsize - 1) in = buffer;
	else in++;
        count++;
	cout << "wirter - " << n+1 << " write 1, buffer " << count << " left\n";
    }
    writing = false;
    cout << "\nwriter - " << n+1 << " completed " << size << " size write\n\n";

    bufferFull->Signal(lock);
    onerequestWrite->Broadcast(lock);

    lock->Release();
    return size;
}

void BoundedBuffer::Close()
{
    bufferEmpty->Broadcast(lock);
    bufferFull->Broadcast(lock);
    onerequestRead->Broadcast(lock);
    onerequestWrite->Broadcast(lock);
    //~BoundedBuffer();
}