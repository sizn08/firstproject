#include <iostream>
#include "dllist.h"
#include <cstddef>
DLLElement::DLLElement(void*itemPtr, int sortKey)
{
	item = itemPtr;
	key = sortKey;
	next = NULL;
	prev = NULL;
}

DLList::DLList()
{
	first = last = NULL;
}

bool
DLList::IsEmpty() 
{ 
    if (first == NULL)
        return TRUE;
    else
	return FALSE; 
}

DLList::~DLList()
{
	if(!IsEmpty()) 
	{ 
		while(first!=last) 
		{ 
			DLLElement *p=first; 
			first=first->next; 
			delete p; 
		} 
		delete first; 
	}
}

void
DLList::Prepend(void *item)
{
    if (IsEmpty()) {
		DLLElement *element = new DLLElement(item, 0);
		first = element;
		last = element;
    } else {
		DLLElement *element = new DLLElement(item, (first->key-1));
		element->next = first;
		first->prev = element;
		first = element;
    }
}

void
DLList::Append(void*item)
{
	if (IsEmpty()){
		DLLElement *element = new DLLElement(item, 0);
		first = element;
		last = element;
	}else{
		int key = last->key+1;
		DLLElement *element = new DLLElement(item,key);
		last->next = element;
		element->prev = last;
		last = element;
	}
}

void *
DLList::Remove(int *keyPtr)
{
	if(IsEmpty()) 
	{ 
		keyPtr=NULL; 
		return NULL; 
	} 
	else 
	{ 
		*keyPtr = first->key; 
		return SortedRemove(*keyPtr); 
	} 
}

void
DLList::SortedInsert(void *item, int sortKey)
{
    DLLElement *element = new DLLElement(item, sortKey);
	if(IsEmpty()) 
	{ 
		first=last=element; 
	} 
	else 
	{ 
		if(first->key>sortKey) 
		{
			element->next=first; 
			first->prev=element; 
			element->prev=NULL; 
			first=element; 
		} 
		else 
		{ 
			DLLElement* head=first; 
			while(head->next!=NULL) 
			{ 
				if(sortKey<head->next->key) 
				{ 
					element->next=head->next; 
					element->prev=head; 
					head->next->prev=element; 
					head->next=element; 
					return; 
				} 
				else 
					head=head->next; 
			} 
			head->next=element; 
			element->prev=head; 
			element->next=NULL; 
			last=last->next; 
		} 
	} 
} 

void *
DLList::SortedRemove(int sortkey)
{	if(IsEmpty())return NULL; 
		DLLElement * head1=first; 
		DLLElement * head2=first; 
		while((head1!=NULL)&&(head1->key!=sortKey)) 
		{ 
			head2=head1; 
			head1=head1->next; 
		} 
		if(head1==NULL)return NULL; 
		else if(head1==first) 
		{ 
			DLLElement *rm=first; 
			void *thing=first->item; 
			first=first->next; 
	        if(first==NULL) 
			{ 
				first=last=NULL; 
				delete rm; 
				return thing; 
			} 
			else 
			{ 
				first->prev=NULL; 
				delete rm; 
				return thing; 
			} 
		} 
		else 
		{ 
			void *thing=head1->item; 
			head2->next=head1->next; 
			if(head1->next!=NULL) 
				head1->next->prev=head2; 
			else last=head2; 
			delete head1; 
			return thing; 
		}
}